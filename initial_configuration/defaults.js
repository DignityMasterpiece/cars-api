module.exports = [
  {
    "description": " change of suspension",
    "make": "Nissan",
    "model": "Versa",
    "estimateDate": "2021/12/01",
    "id": 3340,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/sentra.jpg"
  }, {
    "description": " motor adjustment",
    "make": " Honda ",
    "model": "CR-V",
    "estimateDate": "",
    "id": 3341,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/cr-v.jpeg"
  },
  {
    "description": " engine tuning ",
    "make": "Honda",
    "model": "Civic",
    "estimateDate": "2020/20/12",
    "id": 3342,
    "km": 90000,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/civic.png"
  },
  {
    "description": " oil change ",
    "make": " Volkswaguen",
    "model": "Tiguan",
    "km": 13500,
    "id": 3343,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/tiguan.png"
  },
  {
    "description": " change of pads ",
    "make": " Nissan ",
    "model": "Sentra",
    "km": 6000,
    "id": 3344,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/sentra.jpg"
  },
  {
    "description": " change of pads ",
    "make": "Volkswagen",
    "model": "Vento",
    "km": 80050,
    "id": 3345,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/vento.png"
  },
  {
    "description": "Change Transmission (CVT)",
    "make": "Chevrolet",
    "model": "Aveo NG",
    "estimateDate": "2021/09/07",
    "km": 33460,
    "id": 3346,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/aveo.webp"
  },
  {
    "description": "Change ligths",
    "make": "Chevrolet",
    "model": "Spark",
    "km": 16098,
    "id": 3347,
    "image": "https://cart-bucket.s3.us-east-2.amazonaws.com/cars/spark.webp"
  }
]
