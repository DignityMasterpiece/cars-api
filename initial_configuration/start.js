// dependences
import chalk from "chalk";
// connection files
import {
  mongoConnect,
  mongoDisconnect } from "../src/config/database";
import Car from '../src/models/carModel'
import initialInformation from "./defaults";
(async () => {
  try {
    mongoConnect();
    const inserted = await Car.insertMany(initialInformation)
    console.log(chalk.green(`inserted result${inserted}`))
    mongoDisconnect();
  } catch (error) {
    console.log(
      chalk.redBright(error)
    )
  }
})();
