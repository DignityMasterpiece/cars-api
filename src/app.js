import express from 'express';
import fs from 'fs';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import { deploy } from './config/environment';
import errorHandler from './middlewares/errorHandler';
import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';
import { mongoConnect } from './config/database';
import notFoundResponse from './utils/responses/mockups/default/notFoundResponse'
import url from 'url'
const routes = fs.readdirSync(__dirname + '/routes');
const router = express.Router();
const app = express();
//Settings
app.set('port', process.env.port || deploy.port);
mongoConnect()
//Middlewares
app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(
  cors({
    credentials: true,
    origin: '*'
  })
);
routes.forEach(versionFolder => {
  const version = versionFolder.split('.')[0];
  const routes = fs.readdirSync(__dirname + '/routes/' + version)
  routes.forEach(file => {
    const resource = file.split('.')[0];
    const baseUrl = resource === 'index' ? `/api/${version}/` : `/api/${version}/${resource}/`;
    console.log(baseUrl)
    app.use(baseUrl, [], require(`./routes/${versionFolder}/${file}`));
  })
});
app.use(
  router.all('*', (req, res) =>
    res.status(404).send(notFoundResponse(url.format({
      protocol: req.protocol,
      host: req.get('host'),
      pathname: req.originalUrl
    })))
  )
);
if (app.get('env') === 'development') {
  app.use(errorHandler.developmentErrorHandler); // development error handler
} else {
  app.use(errorHandler.productionErrorHandler); // production error handler
}

module.exports = app;