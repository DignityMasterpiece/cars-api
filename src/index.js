import app from './app'
const port = app.get("port");
//init
app
  .listen(port, () => {
    console.log(`server start on port ${port} - environment ${app.get("env")}`);
  })
  .setTimeout(50000);
