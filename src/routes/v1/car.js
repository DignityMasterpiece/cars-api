import express from 'express';
import CarController from '../../controllers/carController';
import maintenanceValidator from '../../middlewares/requestValidators/maintenanceValidator'
const route = express.Router();
route.get('/', [], CarController.get)
route.patch('/maintenance', [ maintenanceValidator ], CarController.maintenance)
module.exports = route;