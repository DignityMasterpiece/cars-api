import Validator from 'validatorjs';
//'number': ['required', 'regex:/^[0-9]{10}$/'],
module.exports = async (req, res, next) => {
  const { body } = req;
  try {
    let rules = {
      'id': 'required|integer',
      'description': 'required|string',
      'owner': 'required|string',
      'estimateDate': ['required', 'regex:/^[0-9]{4}(/)(((0)[0-9])|((1)[0-2]))(/)([0-2][0-9]|(3)[0-1])$/'],
    };

    const validator = new Validator(body, rules);
    const errors = validator.errors
    validator.checkAsync(() => {
      return next();
    }, () => {
      return res.status(422).send({
        success: false,
        message: 'Invalid fields.',
        errors,
      })
    });
  } catch (error) {
    return res.status(500).send({
      success: false,
      message: 'Internal Error.',
      error: error.message
    })
  }
};