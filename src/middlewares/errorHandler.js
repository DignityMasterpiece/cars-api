module.exports = {

  developmentErrorHandler: (err, req, res, next) => {
    // Shows stack error
    let status = err.status || 500;
    res.status(status).send({
      message: err.message,
      success: false,
      stack: err.stack,
      errors: err.errors || undefined
    });
    return;
  },

  productionErrorHandler: (err, req, res, next) => {
    // Shows only error message
    let status = err.status || 500;
    res.status(status).send({
      message: err.message,
      success: false,
      errors: err.errors || undefined
    });
    return;
  }
}
