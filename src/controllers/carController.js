import Car from '../models/carModel';
import getResponse from '../utils/responses/mockups/car/get'
import maintenanceResponse from '../utils/responses/mockups/car/maintenance'
import getAggregate from '../utils/database/querys/car/get'
exports.get = async (req, res, next) => {
  try {
    const catalog = await Car.aggregate(getAggregate())
    res.send(getResponse(catalog))
  } catch (error) {
    next(error);
  }
};
exports.maintenance = async (req, res, next) => {
  try {
    const { body: { id, description, owner, estimateDate } } = req;
    const filter = { id };
    const update = {
      $set:
        { description, owner, estimateDate, inMaintenance: true }
    };
    const config = { new: true };
    const updated = await Car.findOneAndUpdate(filter, update, config)
    res.send(maintenanceResponse(updated))
  } catch (error) {
    next(error);
  }
};
