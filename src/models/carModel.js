import mongoose from 'mongoose'
const carSchema = new mongoose.Schema({
  id: { type: Number, index: true, unique: true },
  make: String,
  model: String,
  estimateDate: String,
  km: Number,
  image: String,
  description: String,
  owner: String,
  inMaintenance: {
    type: Boolean,
    default: false
  }
}, { collection: 'car' })
  .index({
    id: 1
  }, { sparse: true, index: true })
module.exports = mongoose.model('Car', carSchema)