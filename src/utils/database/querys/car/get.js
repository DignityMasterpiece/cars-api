export default () => {
  return [
    {
      $match: {}
    }, {
      $project: {
        id: 1,
        make: 1,
        model: 1,
        estimateDate: 1,
        image: 1,
        description: 1,
        owner: 1,
        inMaintenance: 1,
        _id:0
      }
    }
  ]
};