export default url => {
  return {
    success: false,
    message: `${url} Not found`,
  };
};
