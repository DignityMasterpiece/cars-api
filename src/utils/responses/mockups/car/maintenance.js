export default result => {
  const { id, owner, model } = result;
  return {
    success: true,
    message: 'Maintenance registry generated successfully.',
    result:{
      id, 
      owner,
      model 
    }
  };
};
