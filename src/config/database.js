import mongoose from 'mongoose';
import { mongodb } from './environment';

const mongoConnect = async () => {
  try {
    const { domain, database, port } = mongodb;
    let connectionString = `mongodb://${domain}:${port}/${database}`;
    await mongoose
      .connect(connectionString, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      })
  } catch (error) {
    throw error
  }
};
const mongoDisconnect = () => {
  mongoose.disconnect()
};

module.exports = {
  mongoConnect,
  mongoDisconnect
};
