# CARS - API
**Examen**.

Este proyecto desarrollado en *NODE.JS* y 
contenerizado mediante *DOCKER*,


## Prerequisites
Es necesario instalar los siguientes paquetes:


### Develop
- [![N|Solid](https://avatars3.githubusercontent.com/u/9950313?s=20&v=4) Node.js](https://nodejs.org/es/)  v10.15.3
- [![Build Status](https://avatars1.githubusercontent.com/u/45120?s=20&v=4) MongoDB](https://www.mongodb.com/)  v4.0.9

### Production
  - [Docker](https://www.docker.com) v19.03.13

---

## Getting Started
1.- Debe crear la carpeta en la que se sincronizara la informacion con MongoDB para evitar perdida de información (en esta guia se tomara como ejemplo el path ***/Users/HP/Projects/datahub***) en caso de crash.

2.- Descargar el repositorio:
***`git clone https://DignityMasterpiece@bitbucket.org/DignityMasterpiece/cars-api.git`***.

3.- Entrar a la carpeta principal del proyecto donde haya sido descargado ***'./cars-api/deployment/'***,
localizada dentro del proyecto.

4.- Ejecturar el comando: ***`cp demo.env .env`***. dentro del archivo debe configurar las variables de su  ambiente.

### Installing
Para realizar el despliegue del proyecto se debe de ejecutar el comando ***`docker-compose up -d`*** dentro de la carpeta ***'./cars-api/deployment/'***,

### Running the Test
  TODO: 

---

## Versioning

Usamos [SemVer](http://semver.org/) para el control de versiones. Para conocer las versiones disponibles, consulte el [tags on this repository](https://github.com/your/project/tags).

## Author

* Jose Luis Hernandez

---

## License

MIT

